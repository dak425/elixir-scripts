#! /usr/bin/env elixir

Mix.install([
  {:req, "~> 0.3.0"},
  {:dotenvy, "~> 0.8.0"},
  {:jason, "~> 1.4"}
])

require Logger

defmodule Util do
  def maybe_to_atom(token) do
    String.to_existing_atom(token)
  rescue
    _ ->
      Logger.debug(fn -> "Converting string to non-existing atom: #{token}" end)
      String.to_atom(token)
  end
end

defmodule Dropbox do
  @doc """
  Computes the content hash of a file based on the Dropbox way of doing so.
  """
  @spec compute_content_hash(File.Stream.t() | String.t()) :: String.t()
  def compute_content_hash(path) when is_binary(path) do
    File.stream!(path, [], 4 * 1024 * 1024) |> compute_content_hash()
  end

  def compute_content_hash(%File.Stream{} = stream) do
    stream
    |> Enum.map(&:crypto.hash(:sha256, &1))
    |> Enum.join()
    |> (&:crypto.hash(:sha256, &1)).()
    |> Base.encode16(case: :lower)
  end
end

defmodule Twitch.User do
  @moduledoc """
  Represents a user obtained from the Twitch API and functions for interacting with them.
  """

  defstruct [
    :id,
    :login,
    :display_name,
    :type,
    :broadcaster_type,
    :description,
    :profile_image_url,
    :offline_image_url,
    :email,
    :created_at
  ]

  @type id :: String.t()

  @type login :: String.t()

  @type display_name :: String.t()

  @type type :: :admin | :global_mod | :staff | :normal

  @type broadcaster_type :: :affiliate | :partner | :normal

  @type description :: String.t()

  @type profile_image_url :: URI.t()

  @type offline_image_url :: URI.t()

  @type email :: String.t() | nil

  @type created_at :: DateTime.t()

  @type t :: %__MODULE__{
          id: id(),
          login: login(),
          display_name: display_name(),
          type: type(),
          broadcaster_type: broadcaster_type(),
          description: description(),
          profile_image_url: profile_image_url(),
          offline_image_url: offline_image_url(),
          email: email(),
          created_at: created_at()
        }

  @valid_user_types ["admin", "global_mod", "staff"]
  @valid_broadcaster_types ["affiliate", "partner"]

  defguard is_valid_user_type(user_type)
           when is_binary(user_type) and user_type in @valid_user_types

  defguard is_valid_broadcaster_type(broadcaster_type)
           when is_binary(broadcaster_type) and broadcaster_type in @valid_broadcaster_types

  @doc """
  Converts a string representation of a Twitch user type into the corresponding atom.
  """
  @spec user_type_from_string(String.t()) :: type()
  def user_type_from_string(""), do: :normal

  def user_type_from_string(user_type) when is_valid_user_type(user_type),
    do: String.to_existing_atom(user_type)

  @doc """
  Converts a string representation of a Twitch broadcaster type into the corresponding atom.
  """
  @spec broadcaster_type_from_string(String.t()) :: broadcaster_type()
  def broadcaster_type_from_string(""), do: :normal

  def broadcaster_type_from_string(broadcaster_type)
      when is_valid_broadcaster_type(broadcaster_type),
      do: String.to_existing_atom(broadcaster_type)

  @doc """
  Converts a map (usually obtained from the API) into the module struct.
  """
  @spec from_map(map()) :: t()
  def from_map(%{} = map) do
    converted_map =
      map
      |> Map.delete("view_count")
      |> Map.new(fn {k, v} -> {Util.maybe_to_atom(k), v} end)
      |> Map.update(:type, nil, &Twitch.User.user_type_from_string/1)
      |> Map.update(:broadcaster_type, nil, &Twitch.User.broadcaster_type_from_string/1)
      |> Map.update(:profile_image_url, nil, &URI.parse/1)
      |> Map.update(:offline_image_url, nil, &URI.parse/1)
      |> Map.update(:created_at, nil, fn created_at ->
        {:ok, date, _} = DateTime.from_iso8601(created_at)
        date
      end)

    struct(__MODULE__, converted_map)
  end
end

defmodule Twitch.Clip do
  @moduledoc """
  Reprents a user's clip from the Twitch API and functions for interacting with them.
  """

  defstruct [
    :id,
    :url,
    :embed_url,
    :broadcaster_id,
    :broadcaster_name,
    :creator_id,
    :creator_name,
    :video_id,
    :game_id,
    :language,
    :title,
    :view_count,
    :created_at,
    :thumbnail_url,
    :duration,
    :vod_offset
  ]

  @type id :: String.t()

  @type url :: URI.t()

  @type embed_url :: URI.t()

  @type broadcaster_id :: String.t()

  @type broadcaster_name :: String.t()

  @type creator_id :: String.t()

  @type creator_name :: String.t()

  @type video_id :: String.t()

  @type game_id :: String.t()

  @type language :: String.t()

  @type title :: String.t()

  @type view_count :: non_neg_integer()

  @type created_at :: DateTime.t()

  @type thumbnail_url :: URI.t()

  @type duration :: float()

  @type vod_offset :: non_neg_integer() | nil

  @type t :: %__MODULE__{
          id: id(),
          url: url(),
          embed_url: embed_url(),
          broadcaster_id: broadcaster_id(),
          broadcaster_name: broadcaster_name(),
          creator_id: creator_id(),
          creator_name: creator_name(),
          video_id: video_id(),
          game_id: game_id(),
          language: language(),
          title: title(),
          view_count: view_count(),
          created_at: created_at(),
          thumbnail_url: thumbnail_url(),
          duration: duration(),
          vod_offset: vod_offset()
        }

  @doc """
  Uses the thumbnail url from a clip to determine the url for the actual video file.
  """
  @spec get_video_file_url(t()) :: URI.t()
  def get_video_file_url(%__MODULE__{thumbnail_url: thumbnail_url}) do
    thumbnail_url
    |> URI.to_string()
    |> String.replace(~r/-preview-*.+/, ".mp4")
    |> URI.parse()
  end

  @doc """
  Sorts a list of clips by the date they were created.
  """
  @spec sort_by_date_created(list(t()), :desc | :asc) :: list(t())
  def sort_by_date_created(clips, order \\ :desc),
    do: Enum.sort_by(clips, & &1.created_at, {order, DateTime})

  @doc """
  Sorts a list of clips by the amount of views each clip has accumulated.
  """
  @spec sort_by_view_count(list(t()), :desc | :asc) :: list(t())
  def sort_by_view_count(clips, order \\ :desc), do: Enum.sort_by(clips, & &1.view_count, order)

  @doc """
  Converts a map (proably from the API) into the module struct.
  """
  @spec from_map(map()) :: t()
  def from_map(%{} = map) do
    converted_map =
      map
      |> Map.new(fn {k, v} -> {Util.maybe_to_atom(k), v} end)
      |> Map.update(:url, nil, &URI.parse/1)
      |> Map.update(:embed_url, nil, &URI.parse/1)
      |> Map.update(:created_at, nil, fn created_at ->
        {:ok, date, _} = DateTime.from_iso8601(created_at)
        date
      end)
      |> Map.update(:thumbnail_url, nil, &URI.parse/1)

    struct(__MODULE__, converted_map)
  end
end

# Load env vars
Dotenvy.source([".env", System.get_env()])

client_id = Dotenvy.env!("TWITCH_CLIENT_ID", :string!)
client_secret = Dotenvy.env!("TWITCH_CLIENT_SECRET", :string!)
channel_name = Dotenvy.env!("TWITCH_CHANNEL", :string!)

dropbox_access_token = Dotenvy.env!("DROPBOX_ACCESS_TOKEN", :string!)
dropbox_upload_path = Dotenvy.env!("DROPBOX_UPLOAD_PATH", :string!)

# Default start date is one week earlier from today or the date stored in a file
default_start_date =
  case File.exists?("last_clip_check.txt") do
    true ->
      File.read!("last_clip_check.txt")

    _ ->
      DateTime.now!("Etc/UTC")
      |> DateTime.add(-1, :week)
      |> DateTime.to_iso8601()
  end

{:ok, clips_start_date, _} =
  System.argv()
  |> OptionParser.parse(strict: [start_date: :string])
  |> elem(0)
  |> Keyword.get(:start_date, default_start_date)
  |> DateTime.from_iso8601()

# Grab access token for API calls
%{status: 200, body: body} =
  Req.post!(
    "https://id.twitch.tv/oauth2/token",
    form: [
      client_id: client_id,
      client_secret: client_secret,
      grant_type: "client_credentials"
    ]
  )

access_token = body["access_token"]

req =
  Req.new(base_url: "https://api.twitch.tv/helix", auth: {:bearer, access_token})
  |> Req.Request.put_header("client-id", client_id)

%{status: 200, body: body} =
  req
  |> Req.get!(
    url: "/users",
    params: [login: channel_name]
  )

user =
  body["data"]
  |> hd()
  |> Twitch.User.from_map()

# Fetch user clips
%{status: 200, body: body} =
  req
  |> Req.get!(
    url: "/clips",
    params: [
      broadcaster_id: user.id,
      started_at: DateTime.to_iso8601(clips_start_date),
      ended_at: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601()
    ]
  )

# Check if any new clips were found
if body["data"] == [] do
  Logger.info("No clips found, exiting...")
  System.halt()
end

# Convert raw maps to module structs and sort by date created
clips =
  body["data"]
  |> Enum.map(&Twitch.Clip.from_map/1)
  |> Twitch.Clip.sort_by_date_created()

# Download the video file for each clip
for clip <- clips do
  video_url = Twitch.Clip.get_video_file_url(clip)
  file_path = "#{clip.title}_#{DateTime.to_iso8601(clip.created_at)}.mp4"
  file = File.open!(file_path, [:write, :exclusive])

  Req.get!(video_url,
    finch_request: fn request, finch_request, finch_name, finch_options ->
      stream_func = fn
        {:status, status}, response ->
          %{response | status: status}

        {:headers, headers}, response ->
          %{response | headers: headers}

        {:data, data}, response ->
          IO.binwrite(file, data)
          response
      end

      case Finch.stream(
             finch_request,
             finch_name,
             Req.Response.new(),
             stream_func,
             finch_options
           ) do
        {:ok, response} -> {request, response}
        {:error, exception} -> {request, exception}
      end
    end
  )

  File.close(file)
end

# Upload each video file to Dropbox
video_files = Path.wildcard("*.mp4")

dropbox_req =
  Req.new(base_url: "https://content.dropboxapi.com/2", auth: {:bearer, dropbox_access_token})
  |> Req.Request.put_header("content-type", "application/octet-stream")

for video <- video_files do
  Logger.info("Uploading '#{video}' to Dropbox...")

  content_hash =
    video
    |> Dropbox.compute_content_hash()
    |> IO.inspect()

  payload = %{
    path: "#{dropbox_upload_path}/#{video}",
    autorename: false,
    strict_conflict: true,
    content_hash: content_hash
  }

  content = File.read!(video)

  dropbox_req
  |> Req.Request.put_header("dropbox-api-arg", Jason.encode!(payload))
  |> Req.post!(url: "/files/upload", body: content)
  |> IO.inspect()
end

# Clean up local video files
for path <- video_files do
  File.rm!(path)
end

# Grab the date off the most recent clip to update the most recent timestamp
# Add one day to it to avoid pulling the same clip
most_recent_clip_date =
  clips
  |> hd()
  |> Map.fetch!(:created_at)
  |> DateTime.add(1, :day)

# Update last time clips were checked
File.write!("last_clip_check.txt", DateTime.to_iso8601(most_recent_clip_date))
